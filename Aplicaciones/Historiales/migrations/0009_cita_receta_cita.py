# Generated by Django 4.2.7 on 2024-02-13 19:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Historiales', '0008_receta'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cita',
            fields=[
                ('idCita', models.AutoField(primary_key=True, serialize=False)),
                ('fechaCreacion', models.DateField()),
                ('doctor', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='Historiales.doctor')),
                ('historial', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='Historiales.historial')),
            ],
        ),
        migrations.AddField(
            model_name='receta',
            name='cita',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='Historiales.cita'),
        ),
    ]
