from django.urls import path
from . import views

urlpatterns=[
    path('listadoRepresentantes/',views.listadoRepresentantes),
    path('guardarRepresentante/',views.guardarRepresentante),
    path('eliminarRepresentante/<id>',views.eliminarRepresentante),
    path('editarRepresentante/<id>', views.editarRepresentante, name='editarRepresentante'),
    path('procesarActualizacionRepresentante/<id>', views.procesarActualizacionRepresentante, name='procesarActualizacionRepresentante'),




    path('listadoDoctores/',views.listadoDoctores),
    path('guardarDoctor/',views.guardarDoctor),
    path('eliminarDoctor/<id>',views.eliminarDoctor),
    path('editarDoctor/<id>', views.editarDoctor, name='editarDoctor'),
    path('procesarActualizacionDoctor/<id>', views.procesarActualizacionDoctor, name='procesarActualizacionDoctor'),


    path('listadoMedicamento/', views.listadoMedicamentos),
    path('guardarMedicamento/', views.guardarMedicamento),
    path('eliminarMedicamento/<id>/', views.eliminarMedicamento),
    path('editarMedicamento/<id>/', views.editarMedicamento, name='editarMedicamento'),
    path('procesarActualizacionMedicamento/<id>/', views.procesarActualizacionMedicamento, name='procesarActualizacionMedicamento'),


    path('listadoPacientes/', views.listadoPacientes, name='listadoPacientes'),
    path('guardarPaciente/', views.guardarPaciente, name='guardarPaciente'),
    path('eliminarPaciente/<int:id>/', views.eliminarPaciente, name='eliminarPaciente'),
    path('editarPaciente/<int:id>/', views.editarPaciente, name='editarPaciente'),
    path('procesarActualizacionPaciente/<int:id>/', views.procesarActualizacionPaciente, name='procesarActualizacionPaciente'),



    path('',views.listadoHistoriales),
    path('guardarHistorial/',views.guardarHistorial),
    path('eliminarHistorial/<id>',views.eliminarHistorial),


    path('listadoCitas/',views.listadoCitas),
    path('guardarCita/',views.guardarCita),
    path('eliminarCita/<idCita>',views.eliminarCita),
    path('editarCita/<idCita>', views.editarCita, name='editarCita'),
    path('procesarActualizacionCita/<idCita>', views.procesarActualizacionCita, name='procesarActualizacionCita'),


    path('listadoRecetas/',views.listadoRecetas),
    path('guardarReceta/',views.guardarReceta),
    path('eliminarReceta/<idReceta>',views.eliminarReceta),
    path('editarReceta/<idReceta>', views.editarReceta, name='editarReceta'),
    path('procesarActualizacionReceta/<idReceta>', views.procesarActualizacionReceta, name='procesarActualizacionReceta'),


    path('listadoDetalles/', views.listadoDetalles),
    path('guardarDetalle/', views.guardarDetalle),
    path('eliminarDetalle/<int:idDetalle>/', views.eliminarDetalle),
    path('editarDetalle/<int:idDetalle>/', views.editarDetalle, name='editarDetalle'),
    path('procesarActualizacionDetalle/<int:idDetalle>/', views.procesarActualizacionDetalle, name='procesarActualizacionDetalle'),



    path('reporte/<id>',views.reporte),


    path('info/<idCita>',views.info),
]
