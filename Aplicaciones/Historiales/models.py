from django.db import models

# Create your models here.
class Representante(models.Model):
    id=models.AutoField(primary_key=True)
    nombreRepresentante=models.CharField(max_length=150)
    apellidoRepresentante=models.CharField(max_length=150)
    cedulaRepresentante=models.CharField(max_length=10)
    telefonoRepresentante=models.CharField(max_length=10)
    nacionalidadRepresentante=models.CharField(max_length=150)
    correoRepresentante=models.EmailField()
    direccionRepresentante=models.TextField()

    def __str__(self):
        fila="- {0}"
        return fila.format(self.nombreRepresentante)


class Doctor(models.Model):
    id=models.AutoField(primary_key=True)
    nombreDoctor=models.CharField(max_length=150)
    apellidoDoctor=models.CharField(max_length=150)
    cedulaDoctor=models.CharField(max_length=10)
    telefonoDoctor=models.CharField(max_length=10)
    fechaNacimientoDoctor=models.DateField()
    especialidadDoctor=models.CharField(max_length=150)
    correoDoctor=models.EmailField()
    fotoDoctor=models.FileField(upload_to='doctores',null=True,blank=True)
    direccionDoctor=models.TextField()

    def __str__(self):
        fila="- {0}"
        return fila.format(self.nombreDoctor)


class Medicamento(models.Model):
    id = models.AutoField(primary_key=True)
    nombreMedicamento = models.CharField(max_length=150)
    dosisMedicamento = models.CharField(max_length=50)
    tipoMedicamento = models.CharField(max_length=150)
    precaucionesMedicamento = models.TextField()

    def __str__(self):
        fila = "- {0}"
        return fila.format(self.nombreMedicamento)



#tabla paciente-relacion con representante
class Paciente(models.Model):
    idPaciente = models.AutoField(primary_key=True)
    nombrePaciente = models.CharField(max_length=150)
    apellidoPaciente = models.CharField(max_length=150)
    cedulaPaciente = models.CharField(max_length=10)
    fechaNacimientoPaciente = models.DateField()
    nacionalidadPaciente = models.CharField(max_length=150)
    representante = models.ForeignKey(Representante, on_delete=models.PROTECT)

    def __str__(self):
        fila = "-{0}"
        return fila.format(self.nombrePaciente)


class Historial(models.Model):
    id = models.AutoField(primary_key=True)
    paciente = models.OneToOneField(Paciente, on_delete=models.PROTECT)
    fechaCreacion = models.DateField()

    def __str__(self):
        fila = "Historial de {1} del {0}"
        return fila.format(self.paciente,self.fechaCreacion)

class Cita(models.Model):
    idCita = models.AutoField(primary_key=True)
    fechaCreacion = models.DateField()
    doctor = models.ForeignKey(Doctor, on_delete=models.PROTECT)
    historial = models.ForeignKey(Historial, on_delete=models.PROTECT)

    def __str__(self):
        fila = "Cita de {0} el {1}"
        return fila.format(self.doctor,self.fechaCreacion)


class Receta(models.Model):
    idReceta = models.AutoField(primary_key=True)
    fechaCreacion = models.DateField()
    altura = models.CharField(max_length=10)
    peso = models.CharField(max_length=10)
    cita = models.OneToOneField(Cita,null=True,blank=True, on_delete=models.PROTECT)


    def __str__(self):
        fila = "Receta creada el -{0}"
        return fila.format(self.fechaCreacion)


class Detalle(models.Model):
    idDetalle = models.AutoField(primary_key=True)
    cantidad = models.CharField(max_length=50)
    frecuencia = models.CharField(max_length=50)
    duracion = models.CharField(max_length=50)
    medicamento = models.ForeignKey(Medicamento, on_delete=models.PROTECT)
    receta = models.ForeignKey(Receta, on_delete=models.CASCADE)    

    def __str__(self):
        fila = "-{0}"
        return fila.format(self.cantidad)
