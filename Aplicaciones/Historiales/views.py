from django.shortcuts import render, redirect
from .models import Representante, Doctor, Medicamento, Paciente, Historial, Receta, Cita, Detalle
from django.contrib import messages
from django.conf import settings
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
# Create your views here.
def listadoRepresentantes(request):
    repreBdd=Representante.objects.all()
    return render(request,'listadoRepresentantes.html',{'representantes':repreBdd})


def guardarRepresentante(request):
    nombreRepresentante=request.POST["nombreRepresentante"]
    apellidoRepresentante=request.POST["apellidoRepresentante"]
    cedulaRepresentante=request.POST["cedulaRepresentante"]
    telefonoRepresentante=request.POST["telefonoRepresentante"]
    correoRepresentante=request.POST["correoRepresentante"]
    nacionalidadRepresentante=request.POST["nacionalidadRepresentante"]
    direccionRepresentante=request.POST["direccionRepresentante"]



    nuevoRepresentante = Representante.objects.create(
        nombreRepresentante=nombreRepresentante,
        apellidoRepresentante=apellidoRepresentante,
        cedulaRepresentante=cedulaRepresentante,
        telefonoRepresentante=telefonoRepresentante,
        correoRepresentante=correoRepresentante,
        nacionalidadRepresentante=nacionalidadRepresentante,
        direccionRepresentante=direccionRepresentante,
    )

    messages.success(request, 'Representante guardado exitosamente')
    send_mail("CREACIÓN DE REPRESENTANTE", "SE HA CREADO "+nombreRepresentante, settings.EMAIL_HOST_USER, ["trabajos.utc.ejemplos@gmail.com"], fail_silently=False )
    return redirect('/listadoRepresentantes')


def eliminarRepresentante(request,id):
    eliminarRepresentante=Representante.objects.get(id=id)
    eliminarRepresentante.delete()
    return redirect('/listadoRepresentantes')

def editarRepresentante(request, id):
    reprEditar = Representante.objects.get(id=id)
    return render(request, 'editarRepresentante.html', {'representante': reprEditar})


def procesarActualizacionRepresentante(request, id):
    id = request.POST["id"]
    nombreRepresentante = request.POST["nombreRepresentante"]
    apellidoRepresentante = request.POST["apellidoRepresentante"]
    cedulaRepresentante = request.POST["cedulaRepresentante"]
    telefonoRepresentante = request.POST["telefonoRepresentante"]
    correoRepresentante = request.POST["correoRepresentante"]
    nacionalidadRepresentante = request.POST["nacionalidadRepresentante"]
    direccionRepresentante = request.POST["direccionRepresentante"]

    reprEditar = Representante.objects.get(id=id)
    reprEditar.nombreRepresentante = nombreRepresentante
    reprEditar.apellidoRepresentante = apellidoRepresentante
    reprEditar.cedulaRepresentante = cedulaRepresentante
    reprEditar.telefonoRepresentante = telefonoRepresentante
    reprEditar.correoRepresentante = correoRepresentante
    reprEditar.nacionalidadRepresentante = nacionalidadRepresentante
    reprEditar.direccionRepresentante = direccionRepresentante
    reprEditar.save()

    messages.success(request, 'Representante actualizado exitosamente')
    return redirect('/listadoRepresentantes')


#Fin de Clases representantes



def listadoDoctores(request):
    doctorBdd=Doctor.objects.all()
    return render(request,'listadoDoctores.html',{'doctores':doctorBdd})


def guardarDoctor(request):
    nombreDoctor=request.POST["nombreDoctor"]
    apellidoDoctor=request.POST["apellidoDoctor"]
    cedulaDoctor=request.POST["cedulaDoctor"]
    telefonoDoctor=request.POST["telefonoDoctor"]
    fechaNacimientoDoctor=request.POST["fechaNacimientoDoctor"]
    especialidadDoctor=request.POST["especialidadDoctor"]
    correoDoctor=request.POST["correoDoctor"]
    direccionDoctor=request.POST["direccionDoctor"]
    fotoDoctor=request.FILES.get("fotoDoctor")

    nuevoDoctor = Doctor.objects.create(
        nombreDoctor=nombreDoctor,
        apellidoDoctor=apellidoDoctor,
        cedulaDoctor=cedulaDoctor,
        telefonoDoctor=telefonoDoctor,
        correoDoctor=correoDoctor,
        especialidadDoctor=especialidadDoctor,
        fechaNacimientoDoctor=fechaNacimientoDoctor,
        direccionDoctor=direccionDoctor,
        fotoDoctor=fotoDoctor,
    )

    messages.success(request, 'Doctor guardado exitosamente')
    send_mail("CREACIÓN DE DOCTOR", "SE HA CREADO "+nombreDoctor, settings.EMAIL_HOST_USER, ["trabajos.utc.ejemplos@gmail.com"], fail_silently=False )
    return redirect('/listadoDoctores')



def eliminarDoctor(request,id):
    eliminarDoctor=Doctor.objects.get(id=id)
    eliminarDoctor.delete()
    return redirect('/listadoDoctores')

def editarDoctor(request, id):
    docEditar = Doctor.objects.get(id=id)
    return render(request, 'editarDoctor.html', {'doctor': docEditar})


def procesarActualizacionDoctor(request, id):
    id = request.POST["id"]
    nombreDoctor=request.POST["nombreDoctor"]
    apellidoDoctor=request.POST["apellidoDoctor"]
    cedulaDoctor=request.POST["cedulaDoctor"]
    telefonoDoctor=request.POST["telefonoDoctor"]
    fechaNacimientoDoctor=request.POST["fechaNacimientoDoctor"]
    especialidadDoctor=request.POST["especialidadDoctor"]
    correoDoctor=request.POST["correoDoctor"]
    direccionDoctor=request.POST["direccionDoctor"]
    fotoDoctor=request.FILES.get("fotoDoctor")

    docEditar = Doctor.objects.get(id=id)
    docEditar.nombreDoctor = nombreDoctor
    docEditar.apellidoDoctor = apellidoDoctor
    docEditar.cedulaDoctor = cedulaDoctor
    docEditar.telefonoDoctor = telefonoDoctor
    docEditar.fechaNacimientoDoctor = fechaNacimientoDoctor
    docEditar.especialidadDoctor = especialidadDoctor
    docEditar.correoDoctor = correoDoctor
    docEditar.fotoDoctor = fotoDoctor
    docEditar.direccionDoctor = direccionDoctor
    docEditar.save()

    messages.success(request, 'Doctor actualizado exitosamente')
    return redirect('/listadoDoctores')

#inicia la clase de medicamento

def listadoMedicamentos(request):
    medicamentos = Medicamento.objects.all()
    return render(request, 'listadoMedicamentos.html', {'medicamentos': medicamentos})


def guardarMedicamento(request):
    if request.method == 'POST':
        nombreMedicamento = request.POST["nombreMedicamento"]
        dosisMedicamento = request.POST["dosisMedicamento"]
        tipoMedicamento = request.POST["tipoMedicamento"]
        precaucionesMedicamento = request.POST["precaucionesMedicamento"]

        Medicamento.objects.create(
            nombreMedicamento=nombreMedicamento,
            dosisMedicamento=dosisMedicamento,
            tipoMedicamento=tipoMedicamento,
            precaucionesMedicamento=precaucionesMedicamento,
        )

    messages.success(request, 'Medicamento guardado exitosamente')
    send_mail("CREACIÓN DE MEDICAMENTO", "SE HA CREADO "+nombreMedicamento, settings.EMAIL_HOST_USER, ["trabajos.utc.ejemplos@gmail.com"], fail_silently=False )
    return redirect('/listadoMedicamento')


def eliminarMedicamento(request, id):
    Medicamento.objects.filter(id=id).delete()
    return redirect('/listadoMedicamento')


def editarMedicamento(request, id):
    medicamento = Medicamento.objects.get(id=id)
    return render(request, 'editarMedicamento.html', {'medicamento': medicamento})



def procesarActualizacionMedicamento(request, id):
    if request.method == 'POST':
        nombreMedicamento = request.POST["nombreMedicamento"]
        dosisMedicamento = request.POST["dosisMedicamento"]
        tipoMedicamento = request.POST["tipoMedicamento"]
        precaucionesMedicamento = request.POST["precaucionesMedicamento"]

        medicamento = Medicamento.objects.get(id=id)
        medicamento.nombreMedicamento = nombreMedicamento
        medicamento.dosisMedicamento = dosisMedicamento
        medicamento.tipoMedicamento = tipoMedicamento
        medicamento.precaucionesMedicamento = precaucionesMedicamento
        medicamento.save()

        messages.success(request, 'Medicamento actualizado exitosamente')
    return redirect('/listadoMedicamento/')




#fin de la clase medicamento

#inicio de la calse pacientes

def listadoPacientes(request):
    pacientes = Paciente.objects.all()
    representantes = Representante.objects.all()
    return render(request, 'listadoPacientes.html', {'pacientes': pacientes, 'representantes': representantes})



def guardarPaciente(request):
    if request.method == 'POST':
        nombrePaciente = request.POST["nombrePaciente"]
        apellidoPaciente = request.POST["apellidoPaciente"]
        cedulaPaciente = request.POST["cedulaPaciente"]
        fechaNacimientoPaciente = request.POST["fechaNacimientoPaciente"]
        nacionalidadPaciente = request.POST["nacionalidadPaciente"]
        representante_id = request.POST["representante_id"]

        nuevoPaciente = Paciente.objects.create(
            nombrePaciente=nombrePaciente,
            apellidoPaciente=apellidoPaciente,
            cedulaPaciente=cedulaPaciente,
            fechaNacimientoPaciente=fechaNacimientoPaciente,
            nacionalidadPaciente=nacionalidadPaciente,
            representante_id=representante_id,
        )

        messages.success(request, 'Paciente guardado exitosamente')
        return redirect('/listadoPacientes')
        send_mail("CREACIÓN DEL PACIENTE", "SE HA CREADO "+nombrePaciente, settings.EMAIL_HOST_USER, ["trabajos.utc.ejemplos@gmail.com"], fail_silently=False )
        return redirect('/listadoPacientes')



def eliminarPaciente(request, id):
    paciente = Paciente.objects.get(idPaciente=id)
    paciente.delete()
    return redirect('listadoPacientes')




def editarPaciente(request, id):
    paciente = Paciente.objects.get(idPaciente=id)
    representantes = Representante.objects.all()
    return render(request, 'editarPaciente.html', {'paciente': paciente, 'representantes': representantes})



def procesarActualizacionPaciente(request, id):
    idPaciente = id
    nombrePaciente = request.POST["nombrePaciente"]
    apellidoPaciente = request.POST["apellidoPaciente"]
    cedulaPaciente = request.POST["cedulaPaciente"]
    fechaNacimientoPaciente = request.POST["fechaNacimientoPaciente"]
    nacionalidadPaciente = request.POST["nacionalidadPaciente"]
    representante_id = request.POST["representante_id"]

    paciente = Paciente.objects.get(idPaciente=idPaciente)
    paciente.nombrePaciente = nombrePaciente
    paciente.apellidoPaciente = apellidoPaciente
    paciente.cedulaPaciente = cedulaPaciente
    paciente.fechaNacimientoPaciente = fechaNacimientoPaciente
    paciente.nacionalidadPaciente = nacionalidadPaciente
    paciente.representante_id = representante_id
    paciente.save()

    messages.success(request, 'Paciente actualizado exitosamente')
    return redirect('/listadoPacientes')






#Historial
def listadoHistoriales(request):
    histoBdd=Historial.objects.all()
    paciBdd=Paciente.objects.all()
    pacientes_sin_historial = Paciente.objects.exclude(historial__isnull=False)
    return render(request,'historiales.html',{'historiales':histoBdd, 'pacientes':pacientes_sin_historial})


def guardarHistorial(request):
    id_paciente = request.POST["id_paciente"]
    paciSeleccionado=Paciente.objects.get(idPaciente=id_paciente)
    fechaCreacion=request.POST["fechaCreacion"]


    nuevoHistorial = Historial.objects.create(
        paciente=paciSeleccionado,
        fechaCreacion=fechaCreacion,
    )

    messages.success(request, 'Historial guardado exitosamente')
    send_mail("CREACIÓN DE HISTORIAL", "SE HA CREADO EL "+fechaCreacion, settings.EMAIL_HOST_USER, ["trabajos.utc.ejemplos@gmail.com"], fail_silently=False )
    return redirect('/listadoHistoriales')

def eliminarHistorial(request,id):
    eliminarHistorial=Historial.objects.get(id=id)
    eliminarHistorial.delete()
    return redirect('/listadoHistoriales')










def listadoRecetas(request):
    receBdd = Receta.objects.all()
    citas_sin_receta = Cita.objects.exclude(receta__isnull=False)
    return render(request, 'listadoRecetas.html', {'recetas': receBdd, 'citas': citas_sin_receta})


def guardarReceta(request):
    fechaCreacion=request.POST["fechaCreacion"]
    altura=request.POST["altura"]
    peso=request.POST["peso"]
    id_cita = request.POST["id_cita"]
    citaSeleccionado=Cita.objects.get(idCita=id_cita)


    nuevoReceta = Receta.objects.create(
        fechaCreacion=fechaCreacion,
        altura=altura,
        peso=peso,
        cita=citaSeleccionado,
    )

    messages.success(request, 'Receta guardada exitosamente')
    send_mail("CREACIÓN DE RECETA", "SE HA CREADO el "+fechaCreacion, settings.EMAIL_HOST_USER, ["trabajos.utc.ejemplos@gmail.com"], fail_silently=False )
    return redirect('/listadoRecetas')

def eliminarReceta(request,idReceta):
    eliminarReceta=Receta.objects.get(idReceta=idReceta)
    eliminarReceta.delete()
    return redirect('/listadoRecetas')

def editarReceta(request, idReceta):
    recetaEditar = Receta.objects.get(idReceta=idReceta)
    citas_sin_receta = Cita.objects.all()
    return render(request, 'editarReceta.html', {'receta': recetaEditar, 'citas': citas_sin_receta})


def procesarActualizacionReceta(request, idReceta):
    idReceta = request.POST["idReceta"]
    fechaCreacion=request.POST["fechaCreacion"]
    peso=request.POST["peso"]
    altura=request.POST["altura"]
    id_cita = request.POST["id_cita"]
    citaSeleccionado=Cita.objects.get(idCita=id_cita)

    recetaEditar = Receta.objects.get(idReceta=idReceta)
    recetaEditar.peso = peso
    recetaEditar.cita = citaSeleccionado
    recetaEditar.fechaCreacion = fechaCreacion
    recetaEditar.altura = altura
    recetaEditar.save()

    messages.success(request, 'Receta actualizada Exitosamente')
    return redirect('/listadoRecetas')




def listadoCitas(request):
    citaBdd=Cita.objects.all()
    docBdd=Doctor.objects.all()
    hisBdd=Historial.objects.all()
    return render(request,'listadoCitas.html',{'citas':citaBdd, 'doctores':docBdd, 'historiales':hisBdd})


def guardarCita(request):
    id_doctor = request.POST["id_doctor"]
    docSeleccionado=Doctor.objects.get(id=id_doctor)
    id_historial = request.POST["id_historial"]
    hisSeleccionado=Historial.objects.get(id=id_historial)
    fechaCreacion=request.POST["fechaCreacion"]


    nuevoCita = Cita.objects.create(
        historial=hisSeleccionado,
        doctor=docSeleccionado,
        fechaCreacion=fechaCreacion,
    )

    messages.success(request, 'Cita guardada exitosamente')
    send_mail("CREACIÓN DE CITA", "SE HA CREADO EL "+fechaCreacion, settings.EMAIL_HOST_USER, ["trabajos.utc.ejemplos@gmail.com"], fail_silently=False )
    return redirect('/listadoCitas')

def eliminarCita(request,idCita):
    eliminarCita=Cita.objects.get(idCita=idCita)
    eliminarCita.delete()
    return redirect('/listadoCitas')


def editarCita(request, idCita):
    citaEditar = Cita.objects.get(idCita=idCita)
    docBdd=Doctor.objects.all()
    hisBdd=Historial.objects.all()
    return render(request, 'editarCita.html', {'cita':citaEditar, 'doctores':docBdd, 'historiales':hisBdd})


def procesarActualizacionCita(request, idCita):
    idCita = request.POST["idCita"]
    id_doctor = request.POST["id_doctor"]
    docSeleccionado=Doctor.objects.get(id=id_doctor)
    id_historial = request.POST["id_historial"]
    hisSeleccionado=Historial.objects.get(id=id_historial)
    fechaCreacion=request.POST["fechaCreacion"]

    citaEditar = Cita.objects.get(idCita=idCita)
    citaEditar.doctor = docSeleccionado
    citaEditar.historial = hisSeleccionado
    citaEditar.fechaCreacion = fechaCreacion
    citaEditar.save()

    messages.success(request, 'Cita actualizada Exitosamente')
    return redirect('/listadoCitas')





def listadoDetalles(request):
    detalles = Detalle.objects.all()
    medicamentos = Medicamento.objects.all()
    recetas = Receta.objects.all()
    return render(request, 'listadoDetalles.html', {'detalles': detalles, 'medicamentos': medicamentos, 'recetas': recetas})


def guardarDetalle(request):
    if request.method == "POST":
        medicamento_id = request.POST.get("id_medicamento")
        medicamento_seleccionado = Medicamento.objects.get(id=medicamento_id)

        receta_id = request.POST.get("id_receta")
        receta_seleccionada = Receta.objects.get(idReceta=receta_id)

        cantidad = request.POST.get("cantidad")
        frecuencia = request.POST.get("frecuencia")
        duracion = request.POST.get("duracion")

        nuevo_detalle = Detalle.objects.create(
            medicamento=medicamento_seleccionado,
            receta=receta_seleccionada,
            cantidad=cantidad,
            frecuencia=frecuencia,
            duracion=duracion,
        )

        messages.success(request, 'Detalle guardado exitosamente')

        send_mail("CREACIÓN DE DETALLE", f"SE HA CREADO UN NUEVO DETALLE\nMedicamento: {medicamento_seleccionado.nombreMedicamento}\nReceta: {receta_seleccionada.fechaCreacion}\nCantidad: {cantidad}\nFrecuencia: {frecuencia}\nDuración: {duracion}", settings.EMAIL_HOST_USER, ["trabajos.utc.ejemplos@gmail.com"], fail_silently=False)
        return redirect('/listadoDetalles')


def eliminarDetalle(request, idDetalle):
    detalle_eliminar = Detalle.objects.get(idDetalle=idDetalle)
    detalle_eliminar.delete()
    return redirect('/listadoDetalles')

def editarDetalle(request, idDetalle):
    detalle_editar = Detalle.objects.get(idDetalle=idDetalle)
    medicamentos = Medicamento.objects.all()
    recetas = Receta.objects.all()
    return render(request, 'editarDetalle.html', {'detalle': detalle_editar, 'medicamentos': medicamentos, 'recetas': recetas})


def procesarActualizacionDetalle(request, idDetalle):
    idDetalle = request.POST["idDetalle"]
    id_medicamento = request.POST["id_medicamento"]
    medicamento_seleccionado = Medicamento.objects.get(id=id_medicamento)
    id_receta = request.POST["id_receta"]
    receta_seleccionada = Receta.objects.get(id=id_receta)
    cantidad = request.POST["cantidad"]
    frecuencia = request.POST["frecuencia"]
    duracion = request.POST["duracion"]

    detalle_editar = Detalle.objects.get(idDetalle=idDetalle)
    detalle_editar.medicamento = medicamento_seleccionado
    detalle_editar.receta = receta_seleccionada
    detalle_editar.cantidad = cantidad
    detalle_editar.frecuencia = frecuencia
    detalle_editar.duracion = duracion
    detalle_editar.save()

    messages.success(request, 'Detalle actualizado exitosamente')
    return redirect('/listadoDetalles')





def reporte(request, id):
    historial = Historial.objects.get(id=id)
    representante = Representante.objects.all()
    paciente = historial.paciente
    cita = Cita.objects.filter(historial=historial)
    doctor = Doctor.objects.all()
    #paciente = Paciente.objects.all()

    return render(request, 'reporte.html', { 'historiales':historial, 'doctores':doctor, 'pacientes':paciente, 'representantes':representante, 'citas': cita})


def info(request, idCita):
    cita = Cita.objects.get(idCita=idCita)
    historial = cita.historial
    paciente = historial.paciente
    representante = paciente.representante
    doctor = cita.doctor
    detalles = Detalle.objects.filter(receta__cita=cita)
    return render(request, 'info.html', {'historial': historial, 'doctor': doctor, 'paciente': paciente, 'representantes': representante, 'cita': cita,'detalles': detalles})
