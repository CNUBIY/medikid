from django.contrib import admin
from .models import Representante, Doctor, Medicamento, Paciente, Historial, Receta, Cita, Detalle
# Register your models here.
admin.site.register(Representante)
admin.site.register(Doctor)
admin.site.register(Medicamento)
admin.site.register(Paciente)
admin.site.register(Historial)
admin.site.register(Receta)
admin.site.register(Cita)
admin.site.register(Detalle)